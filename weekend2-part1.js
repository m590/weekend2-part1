const config = require('dotenv').config();
const express = require('express');
const path = require('path');
require('./api/db/artist.db');
const router = require('./api/router/router');
const userRouter = require('./api/router/user.router');

const app = express();

app.use("/node_modules", express.static(path.join(__dirname, "node_modules")));
app.use(express.static(path.join(__dirname, process.env.public_folder)));

app.use(express.json());
app.use("/api", router);
app.use("/api/user", userRouter);


const server = app.listen(process.env.port, function () {
    console.log('Artists server started at', server.address().port);
});