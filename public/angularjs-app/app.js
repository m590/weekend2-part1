angular.module("app", ['ngRoute', 'angular-jwt'])
    .config(routeConfig);

function routeConfig($routeProvider, $httpProvider) {
    $httpProvider.interceptors.push("AuthenticationInterceptor");
    $routeProvider.when("/", {
        templateUrl: "angularjs-app/welcome/welcome.html",
    }).when("/artists", {
        templateUrl: "angularjs-app/artists-list/artists-list.html",
        controller: "artistController",
        controllerAs: "vm"
    }).when("/artist/:artistId", {
        templateUrl: "angularjs-app/artist-detail/artist-detail.html",
        controller: "artistDetailController",
        controllerAs: "vm"
    }).when("/saveartist", {
        templateUrl: "angularjs-app/artist-save/artist-save.html",
        controller: "artistAddController",
        controllerAs: "vm",
        access: {restricted: true}
    }).when("/register", {
        templateUrl: "angularjs-app/register/register.html",
        controller: "RegisterController",
        controllerAs: "vm",
        access: {restricted: true}
    }).when("/profile", {
        templateUrl: "angularjs-app/profile/profile.html",
        access: {restricted: true}
    });
}