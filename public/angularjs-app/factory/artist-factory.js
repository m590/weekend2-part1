angular.module("app").factory("ArtistFactory", ArtistFactory);

const _url = '/api/artists';

function ArtistFactory($http) {
    return {
        getAllArtists: getAll,
        getArtist: getOne,
        postArtist: addOne,
        deleteArtist: deleteOne,
        updateArtist: updateOne
    }

    function getAll(offset, count) {
        return $http.get(_url + "?offset=" + offset + "&count=" + count).then(complete).catch(failure);
    }

    function getOne(artistId) {
        return $http.get(_url + "/" + artistId).then(complete).catch(failure);
    }

    function addOne(artist) {
        return $http.post(_url, artist).then(complete).catch(failure);
    }

    function deleteOne(artistId) {
        return $http.delete(_url + "/" + artistId).then(complete).catch(failure);
    }

    function updateOne(artist) {
        return $http.put(_url + "/" + artist._id, artist).then(complete).catch(failure);
    }

    function complete(response) {
        return response.data;
    }

    function failure(error) {
        console.log('Artist Factory error', error);
        return error;
    }
}