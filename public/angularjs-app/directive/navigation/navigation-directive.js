angular.module('app').directive("customNavigation", CustomNavigation);

function CustomNavigation(){
    return {
        restrict: "E",
        templateUrl: "angularjs-app/directive/navigation/navigation.html"
    }
}