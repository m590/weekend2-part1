angular.module('app').controller('RegisterController', RegisterController);

function RegisterController(UserFactory) {
    const vm = this;

    vm.user = {};
    vm.register = function () {
        if (vm.user.password !== vm.user.repeatPassword) {
            console.log('register');
            vm.err = "Password must match";
        } else {
            UserFactory.register(vm.user).then(function (response) {
                console.log('response', response);
                if(response.status == 200) {
                    vm.err = '';
                    vm.message = "Success";
                }else{
                    vm.err = response.data;
                }
            }).catch(function (err) {
                vm.err = err;
            });
        }
    }
}