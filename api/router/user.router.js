const express = require('express');
const userCtrl = require('../controller/user.ctrl');
const router = express.Router();

router.route('/register')
    .post(userCtrl.register);

router.route('/login')
    .post(userCtrl.login);

module.exports = router;