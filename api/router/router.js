const express = require('express');
const artistCtrl = require('../controller/artist.ctrl');
const albumCtrl = require('../controller/album.ctrl');
const userCtrl = require('../controller/user.ctrl');

const router = express.Router();

router.route("/artists")
    .get(artistCtrl.getAllArtists)
    .post(userCtrl.authenticate, artistCtrl.addArtist);

router.route("/artists/:artistId")
    .get(artistCtrl.getOneArtist)
    .put(userCtrl.authenticate, artistCtrl.fullUpdateArtist)
    .patch(userCtrl.authenticate, artistCtrl.partialUpdateArtist)
    .delete(userCtrl.authenticate, artistCtrl.deleteArtist);

router.route("/artists/:artistId/albums")
    .get(userCtrl.authenticate, albumCtrl.getAllAlbums)
    .post(userCtrl.authenticate, albumCtrl.addAlbum);

router.route("/artists/:artistId/albums/:albumId")
    .get(userCtrl.authenticate, albumCtrl.getOneAlbum)
    .put(userCtrl.authenticate, albumCtrl.updateAlbum)
    .patch(userCtrl.authenticate, albumCtrl.updatePartialAlbum)
    .delete(userCtrl.authenticate, albumCtrl.deleteAlbum);

module.exports = router;